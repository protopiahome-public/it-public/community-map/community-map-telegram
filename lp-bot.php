<?php

echo "a"; ob_flush(); flush();

require_once __DIR__ . '/vendor/autoload.php';

use Jose\Object\JWK;
use Jose\Factory\JWSFactory;

//use \Firebase\JWT\JWT;

require_once("config.php");
require_once("telegram_functions.php");
require_once("graphql_functions.php");

if ($_REQUEST["secret"] != $telegram_secret)
{
	die();
}

$data = json_decode(file_get_contents('php://input'), true);
file_put_contents(__DIR__ . "/log.txt", print_r($_REQUEST, true), FILE_APPEND);
file_put_contents(__DIR__ . "/log.txt", print_r($data, true), FILE_APPEND);

if ($data["callback_query"]["message"])
{
	$data["message"]["from"] = $data["callback_query"]["from"];
	$data["message"]["chat"] = $data["callback_query"]["message"]["chat"];
}

protopia_auth();

$data["message"]["text"] = preg_replace("#^(\/[a-z0-9\_]+)\@.+?bot#i", "\\1", $data["message"]["text"]);

$session_file = __DIR__ . "/session/" . "lp-telegram" . $data["message"]["from"]["id"] . ".json";
$_SESSION = json_decode(file_get_contents($session_file), true);

if (preg_match("#^\/#", $data["message"]["text"], $matches) && $_SESSION["status"])
{
	$_SESSION = [];
	answer_one("Процесс отменен", null, ["remove_keyboard" => true]);
}

if (!$ecosystem_user_token && (preg_match("#^\/connect (.+)$#s", $data["message"]["text"], $matches) || preg_match("#^\/start (.+)$#s", $data["message"]["text"], $matches)))
{
	$key4 = new JWK([
		'kty' => 'oct',
		'k'   => $ecosystem_client_secret,
	]);
	
	$login_hint_token = array(
		"sub" => $data["message"]["from"]["id"],
		"aud" => [$client_url, $client_url],
		"iss" => $client_url,
		"iat" => time(),
		"exp" => time() + 3600,
		"acr" => "telegram",
		"amr" => "opt",
	);
	$jwt = JWSFactory::createJWSToCompactJSON(
		$login_hint_token,                      // The payload or claims to sign
		$key4,                         // The key used to sign
		['alg' => 'HS256', "kid" => $ecosystem_client_id]
	);
	
	$auth_result = protopia_mutation("authorize", "auth_req_id", ["input: AuthorizeInput!" => 
	array(
		"scope" => "user",
		"user_code" => $matches[1],
//		"login_hint_token" => $jwt,
		"assertion" => $assertion_jwt,
		)
	]);
	
	$token_user_result = protopia_mutation("token", "access_token", ["input: TokenInput!" => 
		array(
			"grant_type" => "ciba",
			"auth_req_id" => $auth_result["auth_req_id"],
			"assertion" => $assertion_jwt,
		)
	]);
	
	$ecosystem_user_token = $token_user_result["access_token"];
	//answer($ecosystem_user_token);
	
	protopia_mutation("changeCurrentUser", "_id", ["input: UserCurrentInput" => [
		"telegram_id" => intval($data["message"]["from"]["id"]),
	]]);
	
	answer("Вы авторизовались!");
	answer(
"/add_place_geo - добавить поляну по геометке
/add_place_address - добавить поляну по адресу
/edit_place - изменить адрес у поляны
/add_event - добавить событие в поляну
/set_chat - установить текущий чат в поляну (Только для групповых чатов. Сначала добавьте бота в чат.)
/check_in - зачекиниться в событии (только в личном диалоге с ботом и на мобильном устройстве)
"
);
}
elseif (preg_match("#^\/help#s", $data["message"]["text"], $matches))
{
	answer(
"/add_place_geo - добавить поляну по геометке
/add_place_address - добавить поляну по адресу
/edit_place - изменить адрес у поляны
/add_event - добавить событие в поляну
/set_chat - установить текущий чат в поляну (Только для групповых чатов. Сначала добавьте бота в чат.)
/check_in - зачекиниться в событии (только в личном диалоге с ботом и на мобильном устройстве)
"
);
}
elseif (false && !$ecosystem_user_token && preg_match("#^\/register$#s", $data["message"]["text"], $matches))
{
	protopia_mutation("registerUser", "_id", ["input: UserInput" => [
		"name" => $data["message"]["from"]["first_name"],
		"family_name" => $data["message"]["from"]["last_name"],
		"vk_id" => $data["message"]["from"]["id"],
	]]);
}
elseif (!$ecosystem_user_token && $data["message"]["chat"]["id"] == $data["message"]["from"]["id"])
{
	answer("@username, добро пожаловать в Лигу Проектов
Чтобы начать использовать бота, зайдите на сайт и авторизуйтесь там: http://movementpeople.ru");
}
if (preg_match("#^\/check_in$#s", $data["message"]["text"], $matches))
{
	$_SESSION["status"] = "check_in_event";
	answer_one("Пришлите геометку", null, ["resize_keyboard" => true, "keyboard" => [
		[
			[
				"text" => "Пришлите геометку",
				"request_location" => true,
			]
		]
	]]);
}
elseif ($data["message"]["location"] && $_SESSION["status"] == "check_in_event")
{
	$_SESSION["status"] = "";
	$event = protopia_mutation("checkInEvent", "_id title", ["check_in: CheckInInput" => [
		"longitude" => floatval($data["message"]["location"]["longitude"]),
		"latitude" => floatval($data["message"]["location"]["latitude"]),
	]]);
	if ($event["title"])
	{
		answer_one("Вы зачекинились на {$event["title"]}!", null, ["remove_keyboard" => true]);
	}
	else
	{
		answer_one("Рядом нет событий!", null, ["remove_keyboard" => true]);
	}
}
elseif (preg_match("#^\/add_place_(geo|address)$#s", $data["message"]["text"], $matches))
{
	$_SESSION["status"] = "place_title";
	$_SESSION["place_method"] = $matches[1];
	answer("Пришлите название вашей поляны");
}
elseif (preg_match("#^\/set_chat$#s", $data["message"]["text"], $matches))
{
	if (!is_admin())
	{
		answer("Вы должны быть админом этого чата!");
		return;
	}
	
	$places = protopia_query("getMyPlaces", "_id title");
	if (!count($places))
	{
		answer("У вас нет полян. Для начала создайте поляну командой /add_place");
		return;
	}

	$_SESSION["status"] = "place_chat";

	answer_one("Укажите поляну", null, generate_lister($places));
}
elseif ($_SESSION["status"] == "place_chat")
{
	$_SESSION = [];
	edit_message_buttons();
	$chat = protopia_mutation("changeChat", "_id", [
		"input:ChatInput" => [
			"external_id" => $data["message"]["chat"]["id"],
			"external_url" => $data["message"]["chat"]["username"] ? "https://t.me/{$data["message"]["chat"]["username"]}" : null,
			"external_type" => $data["message"]["chat"]["id"] == $data["message"]["from"]["id"] ? "personal_chat" : "group_chat",
			"external_system" => "telegram",
			"title" => $data["message"]["chat"]["title"],
		]
	]);
	protopia_mutation("setChatToPlace", "_id", [
		"chat_id:ID" => $chat["_id"],
		"place_id:ID" => $data["callback_query"]["data"],
	]);
	
	answer("Чат привязан!");
}
elseif (preg_match("#^\/token#s", $data["message"]["text"], $matches))
{
	answer($ecosystem_user_token);
}
elseif (preg_match("#^\/add_event#s", $data["message"]["text"], $matches))
{
	
	$places = protopia_query("getMyPlaces", "_id title");
	if (!count($places))
	{
		answer("У вас нет полян. Для начала создайте поляну командой /add_place");
		return;
	}
	$_SESSION["status"] = "place_event_place";
	answer_one("Укажите поляну", null, generate_lister($places));
	//answer($text);
}
elseif ($_SESSION["status"] == "place_event_place")
{
	$_SESSION["status"] = "place_event_title";
	$_SESSION["place"] = $data["callback_query"]["data"];
	edit_message_buttons();
	answer("Пришлите название вашего события");
}
elseif ($_SESSION["status"] == "place_event_title")
{
	$_SESSION["status"] = "place_event_date";
	$_SESSION["event_title"] = $data["message"]["text"];
	answer("Пришлите дату события в формате ДД.ММ.ГГГГ");
}
elseif ($_SESSION["status"] == "place_event_date")
{
	$event = protopia_mutation("changePlaceEvent", "_id", [
		"input:PlaceEventInput" => [
			"start_date" => date("c", strtotime($data["message"]["text"])),
			"title" => $_SESSION["event_title"],
		]
	]);
	protopia_mutation("addEventToPlace", "_id", [
		"event_id:ID" => $event["_id"],
		"place_id:ID" => $_SESSION["place"],
	]);
	answer("Событие создано!");
	$_SESSION = [];
}
elseif ($_SESSION["status"] == "place_title")
{
	$_SESSION["status"] = "place_type";
	$_SESSION["place_title"] = $data["message"]["text"];
	answer_one("Введите тип вашей поляны", null, generate_lister([["_id" => "place", "title" => "Поляна"], ["_id" => "school", "title" => "Школа"]]));
}
elseif ($_SESSION["status"] == "place_type")
{
	$_SESSION["status"] = "place_description";
	$_SESSION["place_type"] = $data["callback_query"]["data"];
	answer("Введите описание вашей поляны");
}
elseif ($_SESSION["status"] == "place_description" && $_SESSION["place_method"] == "geo")
{
	$_SESSION["status"] = "check_in_place";
	$_SESSION["place_description"] = $data["message"]["text"];
	answer_one("Пришлите геометку вашей поляны", null, ["resize_keyboard" => true, "keyboard" => [
		[
			[
				"text" => "Отправить геометку",
				"request_location" => true,
			]
		]
	]]);
}
elseif ($data["message"]["location"] && $_SESSION["status"] == "check_in_place" && $_SESSION["place_method"] == "geo")
{
	$_SESSION["longitude"] = $data["message"]["location"]["longitude"];
	$_SESSION["latitude"] = $data["message"]["location"]["latitude"];
	$address = json_decode(file_get_contents("https://geocode-maps.yandex.ru/1.x/?apikey={$yandex_key}&geocode={$data["message"]["location"]["longitude"]},{$data["message"]["location"]["latitude"]}&format=json"), true);
	$address = $address["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["text"];
	$_SESSION["place_address"] = $address;
	protopia_mutation("changePlace", "_id", ["input: PlaceInput" => [
		"longitude" => $_SESSION["longitude"],
		"latitude" => $_SESSION["latitude"],
		"address" => $_SESSION["place_address"],
		"title" => $_SESSION["place_title"],
		"type" => $_SESSION["place_type"],
		"description" => $_SESSION["place_description"],
	]]);
	answer_one("Поляна создана по адресу {$address}!", null, ["remove_keyboard" => true]);
	$_SESSION = [];
}
elseif ($_SESSION["status"] == "place_description" && $_SESSION["place_method"] == "address")
{
	$_SESSION["status"] = "check_in_place";
	$_SESSION["place_description"] = $data["message"]["text"];
	answer_one("Пришлите адрес вашей поляны");
}
elseif ($_SESSION["status"] == "check_in_place" && $_SESSION["place_method"] == "address")
{
	$address = $data["message"]["text"];
	$address = urlencode($address);
	$location = json_decode(file_get_contents("https://geocode-maps.yandex.ru/1.x/?apikey={$yandex_key}&geocode={$address}&format=json"), true);
	$address = $location["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["text"];
	$location = $location["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["Point"]["pos"];
	$location = explode(" ", $location);
	$_SESSION["longitude"] = $location[0];
	$_SESSION["latitude"] = $location[1];
	$_SESSION["place_address"] = $address;
	protopia_mutation("changePlace", "_id", ["input: PlaceInput" => [
		"longitude" => $_SESSION["longitude"],
		"latitude" => $_SESSION["latitude"],
		"address" => $_SESSION["place_address"],
		"title" => $_SESSION["place_title"],
		"type" => $_SESSION["place_type"],
		"description" => $_SESSION["place_description"],
	]]);
	answer_one("Поляна создана по адресу {$address}!", null, ["remove_keyboard" => true]);
	$_SESSION = [];
}
elseif (preg_match("#^\/edit_place$#s", $data["message"]["text"], $matches))
{
	$places = protopia_query("getMyPlaces", "_id title");
	if (!count($places))
	{
		answer("У вас нет полян. Для начала создайте поляну командой /add_place");
		return;
	}

	$_SESSION["status"] = "change_place_address";

	answer_one("Укажите поляну", null, generate_lister($places));
}
elseif ($_SESSION["status"] == "change_place_address")
{
	$_SESSION["status"] = "change_place_send";
	$_SESSION["place_id"] = $data["callback_query"]["data"];
	answer_one("Пришлите адрес вашей поляны");
}
elseif ($_SESSION["status"] == "change_place_send")
{
	$address = $data["message"]["text"];
	$address = urlencode($address);
	$location = json_decode(file_get_contents("https://geocode-maps.yandex.ru/1.x/?apikey={$yandex_key}&geocode={$address}&format=json"), true);
	$address = $location["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["text"];
	$location = $location["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["Point"]["pos"];
	$location = explode(" ", $location);
	$_SESSION["longitude"] = $location[0];
	$_SESSION["latitude"] = $location[1];
	$_SESSION["place_address"] = $address;
	protopia_mutation("changePlace", "_id", ["id: ID"=> $_SESSION["place_id"], "input: PlaceInput" => [
		"longitude" => $_SESSION["longitude"],
		"latitude" => $_SESSION["latitude"],
		"address" => $_SESSION["place_address"],
	]]);
	answer_one("У поляны изменен адрес на {$address}!", null, ["remove_keyboard" => true]);
	$_SESSION = [];
}

function getUserIdByTelegramId($user)
{
	global $ecosystem_client_auth;
	$ecosystem_client_auth = true;
	$result = protopia_query("getUserByExternalId", "_id", [
		"external_id: String" => $user,
		"external_system: String" => "telegram",
	]);
	$ecosystem_client_auth = false;
	return $result["_id"];
}

function generate_lister ($total) {
    $list = array();
    for ($i=0;$i<count($total); $i++) {
    $list["inline_keyboard"][$i][0] = array("text"=>"{$total[$i]["title"]}", "callback_data"=>"{$total[$i]["_id"]}");
    }
	return $list;
}

function edit_message_buttons ($reply_markup = null, $chat_id = null, $message_id = null) {
	global $data;
	if (!$chat_id)
	{
			if (!$data["message"]["chat"]["id"]) {
			$chat_id = $data["callback_query"]["message"]["chat"]["id"];
			} else {
			$chat_id = $data["message"]["chat"]["id"];
			}
	}
		if (!$message_id) 
		{
			if (!$data["message"]["message_id"]) {
			$message_id = $data["callback_query"]["message"]["message_id"];
			} else {
			$chat_id = $data["message"]["message_id"];
			}
		}
	$answer_data = array(
	"chat_id" => $chat_id,
	"message_id" => $message_id,
	);
	if ($reply_markup)
	{
	$answer_data["reply_markup"] = json_encode($reply_markup);
	}
	$ch = curl_init();
		global $bot_token;
	curl_setopt($ch, CURLOPT_URL,"https://api.telegram.org/bot{$bot_token}/editMessageReplyMarkup");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
		http_build_query($answer_data)
	  );
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);
}

file_put_contents($session_file, json_encode($_SESSION));